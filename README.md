A Cloud Firestore Anotation package

## Usage

A simple usage example:

```dart
import 'package:cloud_firestore_anotation/cloud_firestore_anotation.dart';

@CloudFirestoreDocument()
class User() {
  final String id;
  final String name;
}
```

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://gitlab.com/dipdev.studio/open-source/flutter/colud_firestore_anotation/-/issues
